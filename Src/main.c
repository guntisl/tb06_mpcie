/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "gpio.h"
#include "adc.h"
#include "dac.h"
#include "usb_device.h"
#include "usbd_cdc_if.h"
#include <stdio.h>
#include<string.h>
char  Rxbuffer[10];
uint8_t is_equal, got_match;

uint32_t RxbufPoint = 1, value_point = 0;
uint16_t int_value;
uint8_t compareArray(char array1[], char array2[], uint8_t size);
uint8_t compareArray(char array1[], char array2[], uint8_t size) {
    is_equal = memcmp(array1, array2, size);
    return is_equal;
}

struct commands {
char *name[3];
uint8_t port;
uint8_t mode;
char value[5];
};

char temp[5];
char imon[5];

int main(void) {
  HAL_Init();
  SystemClock_Config();
  gpio_init();
  MX_USB_DEVICE_Init();
  ConfigureDAC();
  SysTick->CTRL  = 0; /* Disable SysTick */
  ConfigureGPIOforADC();
  SetClockForADC();
  ConfigureADC();
  CalibrateADC();
  EnableADC();
  ConfigureDMA();
  ADC1->CR |= ADC_CR_ADSTART; /* start the ADC conversions */
  uint8_t gotCommand = 0;
  uint8_t temp_val, imon_val, mon_1v5;
  char compare[5];
  uint8_t i = 0;
  char buf[] = {0};
  char *pointer_char;
struct commands task = {
  {"!C=", "!L=", "!T?"},
  0,
  0,
  {0, 0, 0, 0, 0}};

// struct commands *ptr_task = &task;

for(;;) {
  if (VCP_read(&buf, 1) != 1)
    continue;
  if ( *buf == '!' ) {
    gotCommand = 1;
    RxbufPoint = 0;
  }
  if (gotCommand == 1) {
      Rxbuffer[RxbufPoint] = *buf;
      VCP_write(&Rxbuffer[RxbufPoint], 1);
            compare[(RxbufPoint)] = Rxbuffer[RxbufPoint];
      if (*buf == '@') {
        value_point = RxbufPoint--;
      }
      if (*buf == '\r' && RxbufPoint > 1) {
        gotCommand = 0;
        VCP_write("\n", 2);
        task.mode = Rxbuffer[4];
        task.port =  Rxbuffer[3];
        for (i = 0; i < 4; i++) {
          task.value[i] = Rxbuffer[value_point+i];
        }
        int_value = strtol(task.value, &pointer_char, 10);
        for (i = 0; i < 3; i++) {
          got_match = compareArray(task.name[i], compare, 3);
          if (got_match == 0) break;
          }
        if ((got_match  == 0)&&(i == 0)) {
          if (task.port == '1') {
            GPIOB->ODR ^= (1 << 3);
          } else if (task.port == '2') {
            GPIOB->ODR ^= (1 << 8);
          }
        } else if ((got_match == 0)&&(i == 1)) {
          DAC->DHR12R1 = int_value;
        } else if ((got_match == 0)&&(i == 2)) {
              if (task.port == '1') {
            VCP_write("Temp1 =  ", 10);
            temp_val = ADC_array[2];
            sprintf(temp, "%d", temp_val);
            VCP_write(temp, 5);
            VCP_write("\n", 2);
              } else if (task.port == '2') {
                VCP_write("Imon =  ", 10);
                imon_val = ADC_array[3];
                sprintf(temp, "%d", imon_val);
                VCP_write(temp, 5);
                VCP_write("\n", 2);
              } else if (task.port == '3') {
                  VCP_write("1V5mon =  ", 10);
                  mon_1v5 = ADC_array[0];
                  sprintf(temp, "%d", mon_1v5);
                  VCP_write(temp, 5);
                  VCP_write("\n", 2);
                }
        }          
        for (i = 0; i < 10 ; i++) {
          if (Rxbuffer[i] == 0) break;
          Rxbuffer[i] = 0;
      }
      RxbufPoint = 0;
      VCP_write("\n", 2);
    }
  }
     RxbufPoint++;
}
}
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */

