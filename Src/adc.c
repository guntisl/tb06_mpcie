/**
 * TB06
 *
 * MikroTik
 */

#include "adc.h"

#include "dac.h"
#include "stm32l0xx.h"

#include <stdint.h>

__INLINE void  ConfigureGPIOforADC(void)
{
  /* (1) Enable the peripheral clock of GPIOA and GPIOB */
  /* (2) Select analog mode for PA4 (reset state) */
  /* (3) Select analog mode for PB1 (reset state) */
 // RCC->IOPENR |= RCC_IOPENR_GPIOAEN | RCC_IOPENR_GPIOBEN; /* (1) */
  GPIOA->MODER |= GPIO_MODER_MODE0; /* (2) */
  GPIOA->MODER |= GPIO_MODER_MODE1; /* (2) */
  GPIOA->MODER |= GPIO_MODER_MODE2; /* (2) */
  GPIOA->MODER |= GPIO_MODER_MODE3;
  GPIOA->MODER |= GPIO_MODER_MODE4;
  //GPIOB->MODER |= GPIO_MODER_MODE1; /* (3) */

   /* (2) */
}


/**
  * Brief   This function enables the clock in the RCC for the ADC
  *         and for the System configuration (mandatory to enable VREFINT)
  * Param   None
  * Retval  None
  */
__INLINE void SetClockForADC(void)
{
  /* (1) Enable the peripheral clock of the ADC */
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN; /* (1) */
}


/**
  * Brief   This function performs a self-calibration of the ADC
  * Param   None
  * Retval  None
  */
__INLINE void  CalibrateADC(void)
{
  /* (1) Ensure that ADEN = 0 */
  /* (2) Clear ADEN */
  /* (3) Set ADCAL=1 */
  /* (4) Wait until EOCAL=1 */
  /* (5) Clear EOCAL */
  if ((ADC1->CR & ADC_CR_ADEN) != 0) /* (1) */
{
    ADC1->CR &= (uint32_t)(~ADC_CR_ADEN);  /* (2) */
  }
  ADC1->CR |= ADC_CR_ADCAL; /* (3) */
  while ((ADC1->ISR & ADC_ISR_EOCAL) == 0) /* (4) */
  {
    /* For robust implementation, add here time-out management */
  }
  ADC1->ISR |= ADC_ISR_EOCAL; /* (5) */
}


/**
  * Brief   This function configures the ADC to convert sequentially 3 channels
  *         in continuous mode.
  *         The conversion frequency is 16MHz
  *         The interrupt on overrun is enabled and the NVIC is configured
  * Param   None
  * Retval  None
  */
__INLINE void ConfigureADC(void)
{
  /* (1) Select HSI14 by writing 00 in CKMODE (reset value) */
  /* (2) Select the continuous mode */
  /* (3) Select CHSEL4, CHSEL9 and CHSEL17 */
  /* (4) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than 17.1us */
  /* (5) Enable interrupts on overrrun */
  /* (6) Wake-up the VREFINT (only for VLCD, Temp sensor and VRefInt) */
  //ADC1->CFGR2 &= ~ADC_CFGR2_CKMODE; /* (1) */
  ADC1->CFGR1 |= ADC_CFGR1_CONT; /* (2)*/
  ADC1->CHSELR = ADC_CHSELR_CHSEL0 | ADC_CHSELR_CHSEL1 \
               | ADC_CHSELR_CHSEL2 |  ADC_CHSELR_CHSEL3; /* (3)*/
  ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; /* (4) */
  ADC1->IER = ADC_IER_OVRIE; /* (5) */

  /* Configure NVIC for ADC */
  /* (1) Enable Interrupt on ADC */
  /* (2) Set priority for ADC */
  NVIC_EnableIRQ(ADC1_COMP_IRQn); /* (1) */
  NVIC_SetPriority(ADC1_COMP_IRQn,0); /* (2) */
}


/**
  * Brief   This function configures the DMA to store the result of an ADC sequence.
  *         The conversion results are stored in N-items array.
  * Param   None
  * Retval  None
  */
__INLINE void ConfigureDMA(void)
{
  /* (1) Enable the peripheral clock on DMA */
  /* (2) Enable DMA transfer on ADC and circular mode */
  /* (3) Configure the peripheral data register address */
  /* (4) Configure the memory address */
  /* (5) Configure the number of DMA tranfer to be performs on DMA channel 1 */
  /* (6) Configure increment, size, interrupts and circular mode */
  /* (7) Enable DMA Channel 1 */
  RCC->AHBENR |= RCC_AHBENR_DMA1EN; /* (1) */
  ADC1->CFGR1 |= ADC_CFGR1_DMAEN | ADC_CFGR1_DMACFG; /* (2) */
  DMA1_Channel1->CPAR = (uint32_t) (&(ADC1->DR)); /* (3) */
  DMA1_Channel1->CMAR = (uint32_t)(ADC_array); /* (4) */
  DMA1_Channel1->CNDTR = NUMBER_OF_ADC_CHANNEL; /* (5) */
  DMA1_Channel1->CCR |= DMA_CCR_MINC | DMA_CCR_MSIZE_0 | DMA_CCR_PSIZE_0 \
                      | DMA_CCR_TEIE | DMA_CCR_CIRC; /* (6) */
  DMA1_Channel1->CCR |= DMA_CCR_EN; /* (7) */

  /* Configure NVIC for DMA */
  /* (8) Enable Interrupt on DMA Channel 1  */
  /* (9) Set priority for DMA Channel 1 */
  NVIC_EnableIRQ(DMA1_Channel1_IRQn); /* (8) */
  NVIC_SetPriority(DMA1_Channel1_IRQn,0); /* (9) */
}


/**
  * Brief   This function enables the ADC
  * Param   None
  * Retval  None
  */
__INLINE void EnableADC(void)
{
  /* (1) Enable the ADC */
  /* (2) Wait until ADC ready if AUTOFF is not set */
  ADC1->CR |= ADC_CR_ADEN; /* (1) */
  if ((ADC1->CFGR1 &  ADC_CFGR1_AUTOFF) == 0)
  {
    while ((ADC1->ISR & ADC_ISR_ADRDY) == 0) /* (2) */
    {
      /* For robust implementation, add here time-out management */
    }
  }
}



/**
  * Brief   This function disables the ADC
  * Param   None
  * Retval  None
  */
__INLINE void DisableADC(void)
{
  /* (1) Ensure that no conversion on going */
  /* (2) Stop any ongoing conversion */
  /* (3) Wait until ADSTP is reset by hardware i.e. conversion is stopped */
  /* (4) Disable the ADC */
  /* (5) Wait until the ADC is fully disabled */
  if ((ADC1->CR & ADC_CR_ADSTART) != 0) /* (1) */
  {
    ADC1->CR |= ADC_CR_ADSTP; /* (2) */
  }
  while ((ADC1->CR & ADC_CR_ADSTP) != 0) /* (3) */
  {
     /* For robust implementation, add here time-out management */
  }
  ADC1->CR |= ADC_CR_ADDIS; /* (4) */
  while ((ADC1->CR & ADC_CR_ADEN) != 0) /* (5) */
  {
    /* For robust implementation, add here time-out management */
  }
}




void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}






#endif /* USE_FULL_ASSERT */

void adc_init(void) {
        
        /* Enable ADC clock */
        RCC->APB2ENR |= (1UL << 9);
        
        /* Calibration Setup */
        if((ADC1->CR & ADC_CR_ADEN) != 0){
                ADC1->CR &= (uint32_t)(~ADC_CR_ADEN);
            }
        
        
        /* Enable ADC */
        ADC1->CR |= (1UL << 0);
        
        /* Wait for ISR bit to set */
       // while((ADC1->ISR & 1) == 0);

}



uint8_t adc_get(uint8_t ADCx) {
    
uint32_t volatile ADC_CONVERT;
	/* GPIOA has different reset parameters */
	if(ADCx == MON_1V5) {

    /* Local Variable */
    
    /* Select which pin will be ADC 0...18 */
    ADC1->CHSELR |= (1UL << 0);
    
    /* Enable Continuous mode */
    ADC1->CFGR1 |= (1UL << 13);
    
    /* Right Aligned data in DR register */
    ADC1->CFGR1 |= (0UL << 5);
    
    /* 12-Bit Resolution */
    ADC1->CFGR1 |= (0Ul << 3);
    
    /* Start Conversion */
    ADC1->CR |= (1UL << 2);
    
    /* Grab the last 12-Bit Data part */
    ADC_CONVERT = ADC1->DR & 0x00000FFF;
    





    }


    
	/* GPIOA has different reset parameters */
    else if(ADCx == OVD_DETECT) {

    
        /* Select which pin will be ADC 0...18 */
        ADC1->CHSELR |= (1UL << 1);
    
        /* Enable Continuous mode */
        ADC1->CFGR1 |= (1UL << 13);
    
        /* Right Aligned data in DR register */
        ADC1->CFGR1 |= (0UL << 5);
    
        /* 12-Bit Resolution */
        ADC1->CFGR1 |= (0Ul << 3);
    
        /* Start Conversion */
        ADC1->CR |= (1UL << 2);
    
        /* Grab the last 12-Bit Data part */
        ADC_CONVERT = ADC1->DR & 0x00000FFF;

    
    }


	/* GPIOA has different reset parameters */
    else if(ADCx == LoadTEMP) {

    
        /* Select which pin will be ADC 0...18 */
        ADC1->CHSELR |= (1UL << 2);
    
        /* Enable Continuous mode */
        ADC1->CFGR1 |= (1UL << 13);
    
        /* Right Aligned data in DR register */
        ADC1->CFGR1 |= (0UL << 5);
    
        /* 12-Bit Resolution */
        ADC1->CFGR1 |= (0UL << 3);
    
        /* Start Conversion */
        ADC1->CR |= (1UL << 2);
    
        /* Grab the last 12-Bit Data part */
        ADC_CONVERT = ADC1->DR & 0x00000FFF;
    
    
//      if (ADC_CONVERT > MAX_TEMP) {
  //         dac_off(Load_Set);
    
//    }
    
    }

        
    else if(ADCx == Load_Imon) {

        /* Select which pin will be ADC 0...18 */
        ADC1->CHSELR |= (1UL << 3);
    
        /* Enable Continuous mode */
        ADC1->CFGR1 |= (1UL << 13);
    
        /* Right Aligned data in DR register */
        ADC1->CFGR1 |= (0UL << 5);
    
        /* 12-Bit Resolution */
        ADC1->CFGR1 |= (0UL << 3);
    
        /* Start Conversion */
        ADC1->CR |= (1UL << 2);
    
        /* Grab the last 12-Bit Data part */
        ADC_CONVERT = ADC1->DR & 0x00000FFF;


    }


        return(ADC_CONVERT);

}


float Voltage_Conversion(void) {




    /* Local Variables */
    float voltage = 0.0;
    float voltage_Per_Division = 1.5/4096;//Supply voltage is known as 3.3.
    
    /* Calculate Voltage */
    voltage = ADC_CONVERT*voltage_Per_Division;
    return(voltage);

     }



void adc_uninit(void) {
    
    /* Check that no conversion is ongoing */
    if((ADC1->CR & ADC_CR_ADSTART) != 0){
           //Stop on-going conversion
          ADC1->CR |= ADC_CR_ADSTP;
    }
        
    while((ADC1->CR & ADC_CR_ADSTP) != 0){
        //Wait until conversion is stopped
    }
          
          /* Disable the ADC */
    ADC1->CR |= ADC_CR_ADDIS; 
    while((ADC1->CR & ADC_CR_ADEN) != 0){
          // Wait until ADC is disabled
    }
}


