/**
 * LED Wristwatch
 *
 * Kevin Cuzner
 */

#include "dac.h"

/**
  * Brief   This function enables the peripheral clocks on GPIO port A
  *         and configures PA4 in Analog mode.
  * Param   None
  * Retval  None
  */
__INLINE void  ConfigureGPIOasAnalog(void)
{
  /* (1) Enable the peripheral clock of GPIOA */
  /* (2) Select analog mode for PA4 (reset value) */
 // RCC->IOPENR |= RCC_IOPENR_GPIOAEN; /* (1) */
 GPIOA->MODER |= GPIO_MODER_MODE4; /* (2) */
}


/**
  * Brief   This function enables the peripheral clocks on DAC
  *         and configures the DAC to be ready to generate signal on DAC1_OUT
  *         as soon as data is written in the DAC_DHR12R1 i.e. 12-bit left aligned data.
  * Param   None
  * Retval  None
  */
 void dac_init(void)
{
  /* (1) Enable the peripheral clock of the DAC */
  /* (2) Enable the DAC ch1 */
  RCC->APB1ENR |= RCC_APB1ENR_DACEN; /* (1) */
  DAC->CR |=  DAC_CR_EN1; /* (2) */
  DAC->DHR12R1 = 0;
}



uint8_t dac_on(uint16_t DACx, uint16_t load) {

    if (DACx == Load_Set) {

        val |= 1;

       DAC->DHR12R1 = load;

    }

  return (val);
}


uint8_t dac_off(uint16_t DACx) {

    if (DACx == Load_Set) {

        val = 0;
        DAC->DHR12R1 = 0;


    }

  return (val);
}


__INLINE void  ConfigureDAC(void)
{
  /* (1) Enable the peripheral clock of the DAC */
  /* (2) Enable the DAC ch1 */
  RCC->APB1ENR |= RCC_APB1ENR_DACEN; /* (1) */
  DAC->CR |=  DAC_CR_EN1; /* (2) */
  DAC->DHR12R1 = 0;
}



