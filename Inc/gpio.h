#ifndef _GPIO_H_
#define _GPIO_H_

#include "stm32l0xx.h"
#include <stdint.h>


#define LED_WPAN         (uint8_t)0
#define LED_WLAN         (uint8_t)1
#define LED_WWAN         (uint8_t)2
#define LED0             (uint8_t)3
#define LED1             (uint8_t)4
#define LED2             (uint8_t)5
#define WAKE             (uint8_t)6
#define SC_EN            (uint8_t)7
#define SC_VSEL          (uint8_t)8
#define SIM_SEL          (uint8_t)9
#define PERST            ((uint8_t)10)
#define W_DISABLE        ((uint8_t)11)
#define OVD_RESET        ((uint8_t)12)
#define SC_RST           ((uint8_t)13)


extern void led_init(uint8_t GPIOx);
extern void led_uninit(uint8_t GPIOx);
extern void led_on(uint8_t GPIOx);
extern void led_off(uint8_t GPIOx);
//extern void gpio_init(uint8_t GPIOx);
extern void gpio_init(void);
extern void gpio_uninit(uint8_t GPIOx);
extern void gpio_on(uint8_t GPIOx);
extern void gpio_off(uint8_t GPIOx);

uint8_t perst(void); 
uint8_t w_disable(void);



#endif //_GPIO_H_
