#ifndef _ADC_H_
#define _ADC_H_

#include "stm32l0xx.h"
#include <stdint.h>

#define SHORT_DELAY 200
#define LONG_DELAY 1000

/* Error codes used to make the red led blinking */
#define ERROR_ADC_OVERRUN 0x01
#define ERROR_DMA_XFER 0x02
#define ERROR_UNEXPECTED_DMA_IT 0x04


#define ERROR_HSI_TIMEOUT 0x10
#define ERROR_PLL_TIMEOUT 0x11
#define ERROR_CLKSWITCH_TIMEOUT 0x12

#define NUMBER_OF_ADC_CHANNEL 4

/* Internal voltage reference calibration value address */
#define VREFINT_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FF80078))


#define MON_1V5            (uint8_t)0
#define OVD_DETECT         (uint8_t)1
#define LoadTEMP           (uint8_t)2
#define Load_Imon          (uint8_t)3

#define MAX_TEMP 150
#define MAX_IMON 1000
uint32_t volatile ADC_CONVERT;


extern void adc_init(void);
extern void adc_uninit(void);
uint8_t adc_get(uint8_t ADCx);
float Voltage_Conversion(void);



uint16_t ADC_array[NUMBER_OF_ADC_CHANNEL]; //Array to store the values coming from the ADC and copied by DMA
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void SetClockForADC(void);
void CalibrateADC(void);
void ConfigureADC(void);
void ConfigureGPIOforADC(void);
void ConfigureDMA(void);
void EnableADC(void);
void DisableADC(void);




#endif //_ADC_H_
