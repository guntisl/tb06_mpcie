/**
 * TB06
 *
 * MikroTik
 */

#ifndef _DAC_H_
#define _DAC_H_

//#include "stm32l0xx.h"
#include <stdint.h>
#include "stm32l0xx.h"

#define Load_Set 0
uint8_t val, x;
uint16_t load;

void SystemClock_Config(void);
void  ConfigureDAC(void);

/* Delay value : short one is used for the error coding, long one (~1s) in case 
   of no error or between two bursts */
#define SHORT_DELAY 200
#define LONG_DELAY 1000

/* Error codes used to make the red led blinking */
#define ERROR_UNEXPECTED_ADC_IT 0x01
#define ERROR_UNEXPECTED_EXT_IT 0x02


/* INCREMENT is the burst step */
#define INCREMENT 5

/* following defines are voltages expressed in millivolts */
#define VDD_APPLI 3300
#define LOWER_DAC_OUT_VOLTAGE (uint32_t) (4096 * 200/VDD_APPLI)
#define HIGHER_DAC_OUT_VOLTAGE (uint32_t)(4095 - LOWER_DAC_OUT_VOLTAGE)
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
static __IO uint32_t Tick;
//volatile uint16_t error = 0xFF;  //use to report errors 
/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void  ConfigureGPIO(void);
void  ConfigureGPIOasAnalog(void);
void  dac_init(void);

uint8_t dac_on(uint16_t DACx, uint16_t load);

uint8_t dac_off(uint16_t DACx);

void ms_delay(uint8_t ms);





#endif //_DAC_H_
